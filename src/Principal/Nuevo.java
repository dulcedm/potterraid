/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Dulce
 * Clase que genera la nueva partida, Pide nombre del usuario y enlaza con Tablero 
 */
public class Nuevo extends JFrame {

    JLabel fondo;

    JTextField texto;
    JButton ir;
    JButton volver;

    public Nuevo() {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setSize(651, 578);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Imagenes/28.png"));
        setIconImage(icon);
        Eventos evento = new Eventos();
        ImageIcon a = new ImageIcon(getClass().getResource("/Imagenes/31.png"));
        fondo = new JLabel(a);
        fondo.setBounds(0, 0, 651, 578);

        texto = new JTextField();
        texto.setBounds(280, 260, 170, 25);
        texto.setBackground(Color.ORANGE);
        
        fondo.add(texto);

        ir = new JButton();
        ir.setBounds(490, 245, 82, 57);
        ir.setBorderPainted(false);
        ir.setContentAreaFilled(false);
        ir.setIcon(new ImageIcon(getClass().getResource("/Imagenes/33.png")));
        ir.addActionListener(evento);
        ir.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/34.png")));
        fondo.add(ir);

        volver = new JButton();
        volver.setBounds(480, 490, 141, 64);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setIcon(new ImageIcon(getClass().getResource("/Imagenes/18.png")));
        volver.addActionListener(evento);
        volver.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/19.png")));
        fondo.add(volver);

        getContentPane().add(fondo);

        setLocationRelativeTo(null);
        this.setVisible(true);

    }//class

    public class Eventos implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == volver) {
                setVisible(false);
                Menu qmenu;
                qmenu = new Menu(false);
            }//if       

            if (e.getSource() == ir) {
                if (!texto.getText().equals("Escribe tu nombre...") &&  texto.getText().length()<8) {
                    setVisible(false);
                    Tablero tablerito;
                    tablerito = new Tablero(texto.getText());

                } else {
                    JOptionPane.showMessageDialog(fondo, "Ingrese el nombre (Maximo 8 caracteres)", "Importante", JOptionPane.WARNING_MESSAGE);
                }//else
            }//if 

        }//constructor

    }//EventosBotones

}//ClassNuevo

