/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 * Clase donde se carga el tablero para una nueva partida
 * A su vez, aqui se desarrolla gran parte del motor lógico del juego, se validan colisiones, generan obstaculos, disparos, movimientos, entre otros.
 *
 * @author Dulce
 */
public class Tablero extends JFrame {

    int ancho = 595;
    int alto = 672;
    int tiempo;
    int minutos;
    int segundos;
    
    JLabel barra;
    JLabel terreno;
    JLabel tiempoJuego;
    
    private final Timer tmrTiempo;
    private final Timer tmrFondo;
    private final Timer tmrDisparos;
    
    Boolean perderVida;
    Boolean genObstaculo;
    
    ArrayList<Obstaculo> obstaculos;
    ArrayList<Disparo> disparos;
    
    Personaje player;
    
    Sonido musica;

    /**
     * Constrcutor de la clase Tablero
     */
    public Tablero(String nombreP) {
        /**
         * Genera la ventana de juego
         */
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setSize(ancho, alto);
        /**
         * Para colocar el icono y sustituir al que tiene Java por defecto
         */
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Imagenes/28.png"));
        setIconImage(icon);

        /**
         * Indica que puede perder vida
         */
        perderVida = true;
        
        /**
         * Indica que puede agregar obstaculo
         */
        genObstaculo = true;
           
        /**
         * Agrega al personaje al panel de juego
         */
                
        player = new Personaje(300, 647);
        player.setNombreJ(nombreP);

        ImageIcon person = new ImageIcon(getClass().getResource("/Imagenes/30.jpg"));
        terreno = new JLabel(person);
        terreno.setBounds(0, 0, ancho, person.getIconHeight());
        
        /**
         * aqui se asignan los obstaculos
         */
        obstaculos = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            GenerarObstaculos();
            terreno.add(obstaculos.get(i));
        }//fondo
        
        /**Inicializa disparo
         */
        disparos = new ArrayList<>();
        /**
         * Barra donde va el tiempo, las vidas, el comustible y los puntos
         */
        barra = new JLabel();
        barra.setBounds(0, 0, ancho, 30);
        barra.setOpaque(true);
        barra.setBackground(Color.DARK_GRAY);
        
        minutos = 1;
        segundos = 60;
        tiempo = 0;
        
        musica = new Sonido();
        
        /**
        *Inicio Timers de tiempo y fondo
        */
        tmrTiempo = new Timer(1000, new ContadorTiempo());  
        tmrFondo = new Timer(80, new ContadorFondo());
        tmrDisparos = new Timer(10, new ContadorDisparos());
        
        tiempoJuego = new JLabel(" Tiempo: 00:00 ");
        tiempoJuego.setBounds(480, 5, 200, 20);
        tiempoJuego.setForeground(Color.WHITE);

        getContentPane().add(terreno);
        getContentPane().add(barra);
        terreno.add(player);
        terreno.add(barra);
        barra.add(tiempoJuego);
        barra.add(player.getNombreJ());
        barra.add(player.getVidasJ());
        barra.add(player.getCombustibleJ());
        barra.add(player.getPuntosJ());

        tmrTiempo.start();
        tmrFondo.start();
        
        addKeyListener(new mover());
        setLocationRelativeTo(null);
        this.setVisible(true);
    }//constructor
    
    public void finalizar(){
        if(minutos == 1 && segundos == 30){
            JOptionPane.showMessageDialog(this, "¡Tiempo completado! Felicidades por sobrevivir.");
            getContentPane().remove(this);
            this.setVisible(false);
            Menu menu = new Menu(true);
            tmrTiempo.stop();
        }//Finalizar juego por fin de tiempo
        if(player.getIntCombustible() <= 0 ){
            player.setIntVidasJ(player.getIntVidas()-1);
            player.setIntCombustible(100);
            repaint();
        }//Pierde vida por falta de combustible
        if(player.getIntVidas() == 0){
            JOptionPane.showMessageDialog(this, "¡Te has quedado sin vidas! Mejor suerte la próxima.");
            getContentPane().remove(this);
            this.setVisible(false);
            Menu menu = new Menu(true);
            tmrTiempo.stop();
            tmrFondo.stop();
            tmrDisparos.stop();
        }
    }
    
    public String TiempoTranscurrido() {
        minutos = tiempo / 60;
        segundos = tiempo % 60;
        String cadena = "";
        if (minutos < 10 && segundos < 10) {
            cadena = " Tiempo: 0" + minutos + ":0" + segundos + " ";
        } else if (minutos < 10 && segundos > 9) {
            cadena = " Tiempo: 0" + minutos + ":" + segundos + " ";
        } else if (minutos > 9 && segundos < 10) {
            cadena = " Tiempo: " + minutos + ":0" + segundos + " ";
        } else if (minutos > 9 && segundos > 9) {
            cadena = " Tiempo: " + minutos + ":" + segundos + " ";
        }
        return cadena;
    }// metodo de tiempo
    
    /**
     * se generan los objetos
     */
    public void GenerarObstaculos() {
        Random rnd = new Random();
        float obstipo, obsy, obsx;
            obsx = rnd.nextFloat()* 3 + 1;
            obsy = rnd.nextFloat() * 3 + 1;
            obstipo = rnd.nextFloat() * 3 + 1;
            
        Obstaculo obj = new Obstaculo((float) obsx, (int) obsy, (int) obstipo, alto, 0);
        obstaculos.add(obj);
    }//Generar obtaculos
    
    public void Colisiones() {
        for (int i = 0; i < obstaculos.size(); i++) {
            if (obstaculos.get(i).getRectangle().intersects(player.getRectangle())) {
                if (obstaculos.get(i).getTipo() == 3) {
                    terreno.remove(obstaculos.get(i));
                    player.setIntCombustible(100);
                    obstaculos.remove(i);
                }else{
                    terreno.remove(obstaculos.get(i));
                    player.setIntVidasJ(player.getIntVidas()-1);
                    obstaculos.remove(i);
                }                
                finalizar();
            }
            repaint();
        }//for
        
        for (int i = 0; i < obstaculos.size(); i++) {
            for(int j = 0; j < disparos.size(); j++){
                if (obstaculos.get(i).getRectangle().intersects(disparos.get(j).getRectangle())) {
                    if (obstaculos.get(i).getTipo() == 3) {
                        terreno.remove(obstaculos.get(i));
                        terreno.remove(disparos.get(j));
                        if (player.getPuntos() > 0) {
                            player.setPuntosJ(player.getPuntos() - 10);
                        }
                        obstaculos.remove(i);
                        disparos.remove(j);
                    }else{
                        terreno.remove(obstaculos.get(i));
                        terreno.remove(disparos.get(j));
                            player.setPuntosJ(player.getPuntos() + 10);
                        obstaculos.remove(i);
                        disparos.remove(j);
                    }                
                }// si hay colision
            }//for disparos
            repaint();
            barra.repaint();
        }//for obstaculos
    }//colisiones

    private class ContadorDisparos implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < disparos.size(); i++) {
                disparos.get(i).setY(disparos.get(i).getY()-1);
                repaint();
            }
            
            Colisiones();
        }
    }
    
    private class ContadorFondo implements ActionListener {

        public void actionPerformed(ActionEvent e)  {
            for (int i = 0; i < obstaculos.size(); i++) {
                obstaculos.get(i).setY(obstaculos.get(i).getY()+3);
                repaint();
            }
            
            if (tiempo % 5 == 0 && genObstaculo && tiempo != 0) {                
                genObstaculo = false;
                
                for (int i = 0; i < 2; i++) {
                    if (tiempo % 5 == 0) {
                        Random rnd = new Random();
                        float obstipo, obsy, obsx;
                        obsx = rnd.nextFloat() * 4 + 1;
                        obsy = 200;
                        obstipo = rnd.nextFloat() * 3 + 1;

                        Obstaculo obj = new Obstaculo((float) obsx, (int) obsy, (int) obstipo, alto, (i+1));
                        obstaculos.add(obj);                
                        terreno.add(obstaculos.get(obstaculos.size()-1));
                        repaint();
                    }
                }
            }
            
            if(!genObstaculo && tiempo % 4 == 0){
                genObstaculo = true;
            }
            
            Colisiones();
        }
    }
    
    public class ContadorTiempo implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            tiempo++;
            if(player.getIntCombustible() - 2 >= 0){
                player.setIntCombustible(player.getIntCombustible() - 2);
            }else{
                player.setIntCombustible(0);
            }
            tiempoJuego.setText(TiempoTranscurrido());
            finalizar();
            repaint(); 
        }//action performed

    }//Class contador tiempo
    
    public class mover extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()) {
                  case KeyEvent.VK_D:
                        Disparo disp = new Disparo((int) player.getX() + 15, (int) player.getY() - 35);
                        disparos.add(disp);                
                        terreno.add(disparos.get(disparos.size()-1));
                        repaint();
                        tmrDisparos.start();
                    break;
                 

                case KeyEvent.VK_RIGHT:
                    if (player.getX() + 10 < 470) {
                        player.setX(player.getX() + 10);
                        player.setLocation(player.getX(), player.getY());
                        player.repaint();
                        repaint();
                        perderVida = true;
                    }//if
                    else{
                        if(perderVida){
                            player.setIntVidasJ(player.getIntVidas()-1);
                            repaint(); 
                            finalizar();
                            perderVida = false;
                        }
                    }
                    break;

                case KeyEvent.VK_LEFT:
                    if (player.getX() - 5 > 74) {
                        player.setX(player.getX() - 10);
                        player.setLocation(player.getX(), player.getY());
                        player.repaint();
                        repaint();
                        perderVida = true;
                    }//if
                    else{
                        if(perderVida){
                            player.setIntVidasJ(player.getIntVidas()-1);
                            repaint(); 
                            finalizar();
                            perderVida = false;
                        }
                    }
                    break;

            }
        }//keyPressed
    }//classTeclas
}//class
