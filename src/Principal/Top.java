package Principal;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
/**
 * 
 * En esta clase se define lo necesario para hacer el Top 10 de mejores puntuaciones: Imagen de fonfo y boton de volver
 */
public class Top extends JFrame {

    JLabel ftop;
    JButton volver;

    public Top() {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setSize(672, 479);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Imagenes/28.png"));
        setIconImage(icon);

        ImageIcon ff = new ImageIcon(getClass().getResource("/Imagenes/40.jpg"));
        ftop = new JLabel(ff);
        ftop.setBounds(0, 0,672, 479);

        Boton tocar = new Boton();

        volver = new JButton();
        volver.setBounds(20, 375, 141, 64);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setIcon(new ImageIcon(getClass().getResource("/Imagenes/18.png")));
        volver.addActionListener(tocar);
        volver.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/19.png")));

        getContentPane().add(ftop);
        ftop.add(volver);

        setLocationRelativeTo(null);
        this.setVisible(true);
    }//constructor

    public class Boton implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            if (event.getSource() == volver) {
                setVisible(false);
                Menu qmenu;
                qmenu = new Menu(false);
            }//if             

        }//constructor

    }//Boton

}//ClassTop
