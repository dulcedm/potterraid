/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * Clase Sonido: Carga e inicializa el fondo musical del proyecto
 * @author Dulce
 */
public class Sonido implements Serializable {
    
    public Sonido() { }

    /**
     *
     * metodo Cargar
     */
    public void CargarS() {
        AudioInputStream flujo = null;
        try {

            URL url = this.getClass().getResource("/sonido/0.wav");
            flujo = AudioSystem.getAudioInputStream(url);
            AudioFormat format = flujo.getFormat();
            DataLine.Info info;
            info = new DataLine.Info(Clip.class, format, (int) flujo.getFrameLength() * format.getFrameSize());
            
            Clip reprod = (Clip) AudioSystem.getLine(info);
            reprod.open(flujo);
            reprod.loop(Clip.LOOP_CONTINUOUSLY);
            reprod.start();
        } catch (UnsupportedAudioFileException ex) {
            System.out.println("Error en carga de audio");
        } catch (IOException ex) {
            System.out.println("Error en carga de audio");
        } catch (LineUnavailableException ex) {
            System.out.println("Error en carga de audio");
        } finally {
            try {
                flujo.close();
            } catch (IOException ex) {

            } catch (NullPointerException ex) {

            }
        }

    }
}
