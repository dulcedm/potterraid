package Principal;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * En esta clase se definen los atributos y metodos que tiene el personaje del juego, se hacen los setter y getter y se le asigna su imagen
 */
public class Personaje extends JLabel {

    private int x;
    private int y;
    private int combustible;
    private int vidas;
    private int puntos;

    private JLabel combustibleJ;
    private JLabel vidasJ;
    private JLabel nombreJ;
    private JLabel puntosJ;

    private ImageIcon jugador;

    public Personaje(int posX, int altoVentana) throws HeadlessException {

        jugador = new ImageIcon(getClass().getResource("/Imagenes/35.png"));
        setIcon(jugador);

        this.x = posX;
        this.y = altoVentana - 66;
        this.setBounds(x, y, jugador.getIconWidth(), jugador.getIconHeight());
        this.setIcon(jugador);

        this.nombreJ = new JLabel(" Nombre: " + this.nombreJ);
        this.nombreJ.setBounds(5, 5, 70, 20);
        this.nombreJ.setForeground(Color.WHITE);

        this.combustible = 100;
        this.combustibleJ = new JLabel(" Combustible: " + this.combustible + "%");
        this.combustibleJ.setBounds(180, 5, 150, 20);
        this.combustibleJ.setForeground(Color.WHITE);

        this.vidas = 3;
        this.vidasJ = new JLabel(" Vidas: " + this.vidas);
        this.vidasJ.setBounds(80, 5, 85, 20);
        this.vidasJ.setForeground(Color.WHITE);
        
        this.puntos = 0;
        this.puntosJ = new JLabel(" Puntos: " + this.puntos);
        this.puntosJ.setBounds(350, 5, 100, 20);
        this.puntosJ.setForeground(Color.WHITE);
    }//constructor

    public void paint(Graphics g) {
        g.drawImage(jugador.getImage(), x, y, jugador.getIconWidth(), jugador.getIconHeight(), null);
        this.setOpaque(false);
        this.setVisible(true);
        super.paintComponent(g);
    }//pain

    /**
     * Metodos Getter
     */
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getIntCombustible() {
        return combustible;
    }

    public int getIntVidas() {
        return vidas;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public JLabel getPuntosJ() {
        return puntosJ;
    }

    public void setPuntosJ(int puntosJ) {
        this.puntos = puntosJ;
        this.puntosJ.setText(" Puntos: " + this.puntos);
    }    
    
    public JLabel getCombustibleJ() {
        return combustibleJ;
    }

    public JLabel getVidasJ() {
        return this.vidasJ;
    }

    public JLabel getNombreJ() {
        return nombreJ;
    }

    public ImageIcon getJugador() {
        return jugador;
    }

    /**
     * Metodos setter
     */
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setIntVidasJ(int vidasJ) {
        this.vidas = vidasJ;
         this.vidasJ.setText(" Vidas: " + this.vidas);
        
    }
    public void setNombreJ(String nombreJ) {
        this.nombreJ.setText(nombreJ);
    }

    public void setJugador(ImageIcon jugador) {
        this.jugador = jugador;
    }

    public void setIntCombustible(int combustibleJ) {
        this.combustible = combustibleJ;
        this.combustibleJ.setText(" Combustible: " + this.combustible + "%");
    }

    
    

     public Rectangle getRectangle() {
        return (new Rectangle(x, y, jugador.getIconHeight(), jugador.getIconWidth()));
    }

}//class

