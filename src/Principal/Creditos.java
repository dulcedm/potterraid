package Principal;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;

/**
 * Clase Créditos: Frame de mostrar créditos
 * fcreditos imagen de fondo 
 * volver boton de regresar
 * @author Dulce
 */
public class Creditos extends JFrame {

    JLabel fcreditos;
    JButton volver;

    public Creditos() {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setSize(672, 675);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Imagenes/28.png"));
        setIconImage(icon);

        ImageIcon ff = new ImageIcon(getClass().getResource("/Imagenes/26.jpg"));
        fcreditos = new JLabel(ff);
        fcreditos.setBounds(0, 0, 672, 709);

        Boton tocar = new Boton();

        volver = new JButton();
        volver.setBounds(20, 575, 141, 64);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setIcon(new ImageIcon(getClass().getResource("/Imagenes/18.png")));
        volver.addActionListener(tocar);
        volver.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/19.png")));

        getContentPane().add(fcreditos);
        fcreditos.add(volver);

        setLocationRelativeTo(null);
        this.setVisible(true);
    }//constructor

    public class Boton implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            if (event.getSource() == volver) {
                setVisible(false);
                Menu qmenu;
                qmenu = new Menu(false);
            }//if             

        }//constructor

    }//Boton

}//ClassCreditos
