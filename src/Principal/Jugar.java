package Principal;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;

/**
 * Pantalla de enlace de nuevo juego
 * game fondo de frame
 * back boton de volver
 * nuevo boton de nueva partida 
 * reanudar boton de reanudar partida
 * @author Dulce
 */

public class Jugar extends JFrame {

    JLabel game;

    JButton back;
    JButton nuevo;
    JButton reanudar;

    public Jugar() {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setSize(672, 709);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Imagenes/28.png"));
        setIconImage(icon);

        ImageIcon ff = new ImageIcon(getClass().getResource("/Imagenes/29.jpg"));
        game = new JLabel(ff);
        game.setBounds(0, 0, 672, 709);

        Boton tocar = new Boton();

        back = new JButton();
        back.setBounds(20, 600, 141, 64);
        back.setBorderPainted(false);
        back.setContentAreaFilled(false);
        back.setIcon(new ImageIcon(getClass().getResource("/Imagenes/18.png")));
        back.addActionListener(tocar);
        back.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/19.png")));

        nuevo = new JButton();
        nuevo.setBounds(170, 190, 349, 81);
        nuevo.setBorderPainted(false);
        nuevo.setContentAreaFilled(false);
        nuevo.setIcon(new ImageIcon(getClass().getResource("/Imagenes/16.png")));
        nuevo.addActionListener(tocar);
        nuevo.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/17.png")));

        reanudar = new JButton();
        reanudar.setBounds(210, 350, 257, 80);
        reanudar.setBorderPainted(false);
        reanudar.setContentAreaFilled(false);
        reanudar.setIcon(new ImageIcon(getClass().getResource("/Imagenes/14.png")));
        reanudar.addActionListener(tocar);
        reanudar.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/15.png")));

        getContentPane().add(game);

        game.add(back);
        game.add(nuevo);
        game.add(reanudar);

        setLocationRelativeTo(null);
        this.setVisible(true);

    }//cons

    public class Boton implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            if (event.getSource() == back) {
                setVisible(false);
                Menu m;
                m = new Menu(false);
            }//if           

            if (event.getSource() == nuevo) {
                setVisible(false);
                Nuevo n = new Nuevo();
            }

            if (event.getSource() == reanudar) {
            }
        }//constructor

    }//Boton

}//ClassJugar
