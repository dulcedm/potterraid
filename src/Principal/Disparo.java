/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *Clase que genera el disparo para combatir a los enemigos
 * Posee su posicion x,y y la imagen del disparo
 * Ademas de los metodos getter t setter y el getRectangle para validar colisiones
 * @author Dulce
 */
public class Disparo extends JLabel {
    /*
     * declaracion de variables
     */
    private int x;
    private int y;
    private ImageIcon disparo;

    /*
     * constructor de la clase
     */
    public Disparo(int posX, int posY) {

        disparo = new ImageIcon(getClass().getResource("/Imagenes/39.png"));
                
        this.x = posX;
        
        this.y = posY;
            
        setIcon(disparo);
        this.setBounds(x, y, disparo.getIconWidth(), disparo.getIconHeight());
        this.setIcon(disparo);

    }//constructorDeObstaculo

    public ImageIcon getImagen() {
        return disparo;
    }

    public void paint(Graphics g) {
        g.drawImage(disparo.getImage(), x, y, disparo.getIconWidth(), disparo.getIconHeight(), null);
        this.setOpaque(false);
        this.setVisible(true);
        super.paintComponent(g);
    }//ClassPaint

    /*
     * metodos getter
     */
    public int getX() {
        return x;
    }//getX

    public int getY() {
        return y;
    }//getY

    /*
     * metodos setter
     */
    public void setX(int x) {
        this.x = x;
    }//setX

    public void setY(int y) {
        this.y = y;
    }//sety

    /*
     * este metodo lo usamos para luego validar colisiones
     */
    public Rectangle getRectangle() {
        return (new Rectangle(x, y, disparo.getIconHeight(), disparo.getIconWidth()));
    }//Rectangle
}//Class Disparo