/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * Clase donde se inicializan los obstaculos y sus atributos
 * x posicion x del obstaculo
 * y posicion y del obstaculo
 * tipo numero que definirá si es sombrero, cupcake o dragon
 * @author Dulce
 */
public class Obstaculo extends JLabel {

    /*
     * declaracion de variables
     */
    private int x;
    private int y;
    private int tipoy;
    private int tipo;
    private ImageIcon obstaculo;

    /*
     * constructor de la clase
     */
    public Obstaculo(float posX, int posY, int tipo, int altoVentana, int numero) {

        this.tipo = tipo;
        this.tipoy = posY;
        switch (this.tipo) {
            case 1: //sombrero
                obstaculo = new ImageIcon(getClass().getResource("/Imagenes/36.png"));
                break;
            case 2: //dragon
                obstaculo = new ImageIcon(getClass().getResource("/Imagenes/37.png"));
                break;
            case 3: //cupcake
                obstaculo = new ImageIcon(getClass().getResource("/Imagenes/38.png"));
                break;
        }//switchTipo
        
        if (posX > 1 && posX <2){
            this.x = 75 + 15;
        } else if (posX > 2 && posX <3) {
            this.x = 155 + obstaculo.getIconWidth() + 5;
        } else if (posX > 3 && posX <4) {
            this.x = 300 + obstaculo.getIconWidth() + 5;
        }else{
            this.x = 460 - obstaculo.getIconWidth();
        }
        
        if(numero == 1){
            if(posX>1){ posX = posX - 1; }
            else{ posX = posX + 1; }
        }else if(numero == 2){
            if(posX<4){ posX = posX + 1; }
            else{ posX = posX - 1; }
        }
        
        if (tipoy == 1) {
            this.y = altoVentana - obstaculo.getIconHeight() - 500;
        } else if (tipoy == 2) {
            this.y = altoVentana - obstaculo.getIconHeight() - 200;
        } else if (tipoy == 3) {
            this.y = altoVentana - 300;
        }//else
        setIcon(obstaculo);
        this.setBounds(x, y, obstaculo.getIconWidth(), obstaculo.getIconHeight());
        this.setIcon(obstaculo);

    }//constructorDeObstaculo

    public ImageIcon getImagen() {
        return obstaculo;
    }

    public void paint(Graphics g) {
        g.drawImage(obstaculo.getImage(), x, y, obstaculo.getIconWidth(), obstaculo.getIconHeight(), null);
        this.setOpaque(false);
        this.setVisible(true);
        super.paintComponent(g);
    }//ClassPaint

    /*
     * metodos getter
     */
    public int getTipoy() {
        return tipoy;
    }

    public int getX() {
        return x;
    }//getX

    public int getY() {
        return y;
    }//getY

    public int getTipo() {
        return tipo;
    }//getTipo

    /*
     * metodos setter
     */
    public void setX(int x) {
        this.x = x;
    }//setX

    public void setY(int y) {
        this.y = y;
    }//sety

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }//setTipo
    /*
     * este metodo lo usamos para luego validar colisiones
     */
    public Rectangle getRectangle() {
        return (new Rectangle(x, y, obstaculo.getIconHeight(), obstaculo.getIconWidth()));
    }//Rectangle

}//classObstaculo
