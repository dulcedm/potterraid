package Principal;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
/**
 * 
 * Se carga la imafen de fondo del Frame Instrucciones, donde se dan las reglas del juego.
 * @author Dulce
 */
public class Instrucciones extends JFrame {

    JLabel fondo;
    JButton volver;

    public Instrucciones() {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setSize(672, 675);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Imagenes/28.png"));
        setIconImage(icon);

        ImageIcon f = new ImageIcon(getClass().getResource("/Imagenes/23.jpg"));
        fondo = new JLabel(f);
        fondo.setBounds(0, 0, 672, 709);

        Boton tocar = new Boton();

        volver = new JButton();
        volver.setBounds(20, 575, 141, 64);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setIcon(new ImageIcon(getClass().getResource("/Imagenes/18.png")));
        volver.addActionListener(tocar);
        volver.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/19.png")));

        getContentPane().add(fondo);
        fondo.add(volver);

        setLocationRelativeTo(null);
        this.setVisible(true);
    }//constructor

    public class Boton implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            if (event.getSource() == volver) {
                setVisible(false);
                Menu pmenu;
                pmenu = new Menu(false);
            }//if volver

        }//constructor

    }//boton
}//ClassInstrucciones
