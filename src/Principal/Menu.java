package Principal;

import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * Clase que define y contiene las opciones del menu
 */

public class Menu extends JFrame {

    JLabel menu;
    JLabel opciones;

    JButton b_ir;
    JButton b_creditos;
    JButton b_instrucciones;
    JButton b_top;
    JButton b_salir;
    JButton b_jugar;
    JButton b_volver;

    public Menu(boolean portada) throws HeadlessException {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("POTTER RAID");

        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Imagenes/28.png"));
        setIconImage(icon);

        Botones handler = new Botones();

        ImageIcon panel = new ImageIcon(getClass().getResource("/Imagenes/0.jpg"));
        menu = new JLabel(panel);
        menu.setBounds(0, 0, 672, 709);

        ImageIcon menup = new ImageIcon(getClass().getResource("/Imagenes/1.jpg"));
        opciones = new JLabel(menup);
        opciones.setBounds(0, -10, 672, 709);

        ImageIcon icon_iniciar = new ImageIcon(getClass().getResource("/Imagenes/21.png"));
        b_ir = new JButton();
        b_ir.setBounds(410, 580, 239, 113);
        b_ir.setBorderPainted(false);
        b_ir.setContentAreaFilled(false);
        b_ir.setIcon(icon_iniciar);
        b_ir.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/22.png")));
        b_ir.addActionListener(handler);

        b_jugar = new JButton();
        b_jugar.setBounds(250, 120, 146, 79);
        b_jugar.setBorderPainted(false);
        b_jugar.setContentAreaFilled(false);
        b_jugar.setIcon(new ImageIcon(getClass().getResource("/Imagenes/4.png")));
        b_jugar.addActionListener(handler);
        b_jugar.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/5.png")));

        b_instrucciones = new JButton();
        b_instrucciones.setBounds(170, 220, 334, 80);
        b_instrucciones.setBorderPainted(false);
        b_instrucciones.setContentAreaFilled(false);
        b_instrucciones.setIcon(new ImageIcon(getClass().getResource("/Imagenes/6.png")));
        b_instrucciones.addActionListener(handler);
        b_instrucciones.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/7.png")));

        b_creditos = new JButton();
        b_creditos.setBounds(230, 320, 211, 81);
        b_creditos.setBorderPainted(false);
        b_creditos.setContentAreaFilled(false);
        b_creditos.setIcon(new ImageIcon(getClass().getResource("/Imagenes/10.png")));
        b_creditos.addActionListener(handler);
        b_creditos.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/11.png")));

        b_top = new JButton();
        b_top.setBounds(240, 420, 199, 81);
        b_top.setBorderPainted(false);
        b_top.setContentAreaFilled(false);
        b_top.setIcon(new ImageIcon(getClass().getResource("/Imagenes/8.png")));
        b_top.addActionListener(handler);
        b_top.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/9.png")));

        b_salir = new JButton();
        b_salir.setBounds(520, 605, 124, 65);
        b_salir.setBorderPainted(false);
        b_salir.setContentAreaFilled(false);
        b_salir.setIcon(new ImageIcon(getClass().getResource("/Imagenes/12.png")));
        b_salir.addActionListener(handler);
        b_salir.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/13.png")));

        b_volver = new JButton();
        b_volver.setBounds(30, 605, 141, 64);
        b_volver.setBorderPainted(false);
        b_volver.setContentAreaFilled(false);
        b_volver.setIcon(new ImageIcon(getClass().getResource("/Imagenes/18.png")));
        b_volver.addActionListener(handler);
        b_volver.setPressedIcon(new ImageIcon(getClass().getResource("/Imagenes/19.png")));

        menu.add(b_ir);

        opciones.add(b_creditos);
        opciones.add(b_jugar);
        opciones.add(b_instrucciones);
        opciones.add(b_top);
        opciones.add(b_salir);
        opciones.add(b_volver);

        if (portada) {
            getContentPane().add(menu);
            setSize(672, 709);
        }//if portada
        else {
            getContentPane().add(opciones);
            setSize(672, 709);
        }//else principal

        setLocationRelativeTo(null);
        this.setVisible(true);
    }//ConstructorClassMenu

    public class Botones implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            if (event.getSource() == b_ir) {
                getContentPane().remove(menu);
                getContentPane().add(opciones);
                setSize(672, 709);
                repaint();
            }//if b_ir

            if (event.getSource() == b_volver) {
                getContentPane().remove(opciones);
                getContentPane().add(menu);
                setSize(672, 709);
                repaint();
            }//if b_volver

            if (event.getSource() == b_salir) {
                System.exit(0);
            }//if b_salir

            if (event.getSource() == b_jugar) {
                setVisible(false);
                Jugar g =  new Jugar();

            }//if b_jugar

            if (event.getSource() == b_instrucciones) {
                setVisible(false);
                Instrucciones in;
                in = new Instrucciones();

            }//if b_instrucciones

            if (event.getSource() == b_top) {
                setVisible(false);
                Top top;
                top = new Top();
                
            }//if b_top

            if (event.getSource() == b_creditos) {
                setVisible(false);
                Creditos c;
                c = new Creditos();

            }//if b_top

        }

    }//classBotones
}//ClassMenu
